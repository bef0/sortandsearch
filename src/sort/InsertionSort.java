package sort;

public class InsertionSort extends Sort {

	@Override
	public void sort(int[] array) {
		for (int i = 0; i < array.length; i++) {
			int wert = array[i];
			int j;
			for (j = i; j > 0 && array[j-1] > wert; j--) {
				array[j] = array[j - 1];
			}
			array[j] = wert;
		}


	}

}
