package sort;

public abstract class Sort {
	public abstract void sort(int[] array);
	
	public String ausgeben(int[] array, boolean ausgeben)
	{
		String str = "";
		for (int i = 0; i < array.length; i++)
			str += i + ": " + array[i] + "\n";
		
		if (ausgeben) System.out.println(str);
		
		return str;
	}
}
