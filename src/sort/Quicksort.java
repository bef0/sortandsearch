package sort;

public class Quicksort extends Sort {

	@Override
	public void sort(int[] array) {
		sort(array, 0, array.length - 1);

	}
	
	public void sort(int[] array, int left, int right)
	{
		if (left < right)
		{
			int teiler = teile(array, left, right);
			sort(array, left, teiler - 1);
			sort(array, teiler + 1, right);
		}
	}
	
	private int teile(int[] array, int left, int right)
	{
		int pivot = array[right];
		int i = 0, 
				j = right - 1;
		
		do {
			
			while(i < right && array[i] < pivot)
				i++;
			
			while(j > left && array[j] >= pivot)
				j--;
			
			if (i < j)
			{
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			
		}while(i < j);
		
		array[right] = array[i];
		array[i] = pivot;
		
		return i;
	}

}
