package sort;

public class SelectionSort extends Sort {

	@Override
	public void sort(int[] array) {
		for (int i = 0; i < array.length; i++)
		{
			int kleinsterWert = i;
			
			for (int j = i; j < array.length; j++)
			{
				if (array[j] < array[kleinsterWert])
				{
					kleinsterWert = j;
				}
			}
			
			int temp = array[i];
			array[i] = array[kleinsterWert];
			array[kleinsterWert] = temp;
		}

	}

}
