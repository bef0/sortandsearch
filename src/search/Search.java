package search;

public abstract class Search {
	public abstract int search(int[] array, int element);
}
