package search;

public class LinearSearch extends Search {

	@Override
	public int search(int[] array, int element) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element)
				return i;
		}
		return -1;
	}

}
