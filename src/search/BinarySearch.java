package search;

import sort.Quicksort;

public class BinarySearch extends Search {

	/**
	 * The list must be sorted!
	 */
	@Override
	public int search(int[] array, int element) {
		if (array == null)
			return -1;
		return search(array, element, 0, array.length - 1);
	}

	public int search(int[] array, int element, boolean sorted) {
		if (array == null)
			return -1;

		if (!sorted) {
			Quicksort sort = new Quicksort();
			sort.sort(array);
		}

		return search(array, element, 0, array.length - 1);
	}

	private int search(int[] array, int element, int left, int right) {
		if (left < right && right < array.length) {
			int teiler = teile(left, right);
			if (teiler == 0)
				return -1;

			int center = left + teiler;
			if (array[center] == element) {
				return center;
			}
			if (center == left || center == right) {
				return -1;
			}
			if (array[center] > element) {
				return search(array, element, left, center);
			}
			return search(array, element, center, right);
		} else {
			return -1;
		}
	}

	private int teile(int left, int right) {
		if (left >= right)
			return -1;

		double difference = right - left;
		double i = difference / 2;
		if (difference % 2 != 0) {
			i++;
		}

		return (int) i;
	}

}
