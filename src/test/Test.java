package test;

import static org.junit.Assert.*;

import java.util.Arrays;

import search.*;
import sort.*;

public class Test {
	
	// Test for sort
	@org.junit.Test
	public void testBubblesort() {
		int[] zahlen = {54, 6165, 161, 1, 1, 2, 5, 10, 99};
		int[] ergebnis = {1, 1, 2, 5, 10, 54, 99, 161, 6165};
		Bubblesort sort = new Bubblesort();
		sort.sort(zahlen);
		
		assertTrue(Arrays.equals(zahlen, ergebnis));
	}
	
	@org.junit.Test
	public void testAusgeben() {
		Bubblesort sort = new Bubblesort();
		int[] zahlen = {1,2,3};
		String str = sort.ausgeben(zahlen, false);
		assertTrue(str.equals("0: 1\n1: 2\n2: 3\n"));
		str = sort.ausgeben(zahlen, true);
		assertTrue(str.equals("0: 1\n1: 2\n2: 3\n"));
		
	}
	
	@org.junit.Test
	public void testInsertionSort() {
		int[] zahlen = {54, 6165, 161, 1, 1, 2, 5, 10, 99};
		int[] ergebnis = {1, 1, 2, 5, 10, 54, 99, 161, 6165};
		InsertionSort sort = new InsertionSort();
		sort.sort(zahlen);
		
		assertTrue(Arrays.equals(zahlen, ergebnis));
	}
	
	@org.junit.Test
	public void testSelectionSort() {
		int[] zahlen = {54, 6165, 161, 1, 1, 2, 5, 10, 99};
		int[] ergebnis = {1, 1, 2, 5, 10, 54, 99, 161, 6165};
		SelectionSort sort = new SelectionSort();
		sort.sort(zahlen);
		
		assertTrue(Arrays.equals(zahlen, ergebnis));
	}
	
	@org.junit.Test
	public void testQuicksort() {
		int[] zahlen = {54, 6165, 161, 1, 1, 2, 5, 10, 99};
		int[] ergebnis = {1, 1, 2, 5, 10, 54, 99, 161, 6165};
		Quicksort sort = new Quicksort();
		sort.sort(zahlen);
		
		assertTrue(Arrays.equals(zahlen, ergebnis));
	}
	
	// Test for search
	@org.junit.Test
	public void testLinearSearch() {
		LinearSearch search = new LinearSearch();
		int[] zahlen = {54, 6165, 161, 1, 1, 2, 5, 10, 99};
		assertTrue(search.search(zahlen, 99) == 8);
		assertTrue(search.search(zahlen, 54) == 0);
		assertTrue(search.search(zahlen, 2) == 5);
		assertTrue(search.search(zahlen, 18) == -1);
	}
	
	@org.junit.Test
	public void testBinarySearch() {
		BinarySearch search = new BinarySearch();
		int[] zahlen = {54, 6165, 161, 1, 1, 2, 5, 10, 99}; // {1, 1, 2, 5, 10, 54, 99, 161, 6165}
		assertTrue(search.search(zahlen, 99, false) == 6);
		assertTrue(search.search(zahlen, 54) == 5);
		assertTrue(search.search(zahlen, 2) == 2);
		assertTrue(search.search(zahlen, 18) == -1);
	}
}
