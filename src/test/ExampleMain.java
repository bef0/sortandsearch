package test;

import search.*;
import sort.*;

public class ExampleMain {

	public static void main(String[] args) {
		// Example for sort
		int[] zahlen = { 54, 6165, 161, 1, 1, 2, 5, 10, 99 };
		// int[] ergebnis = {1, 1, 2, 5, 10, 54, 99, 161, 6165};
		Sort sort = new Bubblesort();
		sort.ausgeben(zahlen, true);
		sort.sort(zahlen);
		sort.ausgeben(zahlen, true);

		resetNumbers(zahlen);
		sort = new InsertionSort();
		sort.ausgeben(zahlen, true);
		sort.sort(zahlen);
		sort.ausgeben(zahlen, true);

		resetNumbers(zahlen);
		sort = new SelectionSort();
		sort.ausgeben(zahlen, true);
		sort.sort(zahlen);
		sort.ausgeben(zahlen, true);

		resetNumbers(zahlen);
		sort = new Quicksort();
		sort.ausgeben(zahlen, true);
		sort.sort(zahlen);
		sort.ausgeben(zahlen, true);

		
		// Example for search
		Search search = new LinearSearch();
		resetNumbers(zahlen);
		System.out.println(search.search(zahlen, 99)); // == 8
		System.out.println(search.search(zahlen, 18)); // == -1

		search = new BinarySearch();
		resetNumbers(zahlen);
		System.out.println(((BinarySearch) search).search(zahlen, 99, false)); // == 6
		System.out.println(search.search(zahlen, 18)); // == -1

	}
	
	private static void resetNumbers(int[] zahlen)
	{
		zahlen[0] = 54;
		zahlen[1] = 6165;
		zahlen[2] = 161;
		zahlen[3] = 1;
		zahlen[4] = 1;
		zahlen[5] = 2;
		zahlen[6] = 5;
		zahlen[7] = 10;
		zahlen[8] = 99;
	}

}
